ARG IMAGEN=ubuntu:focal
ARG MAINTAINER=huezohuezo1990
FROM $IMAGEN
MAINTAINER $MAINTAINER

ENV DEBIAN_FRONTEND noninteractive

ENV TZ=America/El_Salvador

# Set the timezone.
ENV TZ=America/El_Salvador
RUN apt-get update 
RUN apt-get install -y tzdata 
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime 
RUN echo $TZ > /etc/timezone
RUN dpkg-reconfigure -f noninteractive tzdata



# Paquetes de instalación (herramientas básicas, cups, controladores básicos, controladores HP)
RUN apt-get update \
&& apt-get install -y \
  sudo \
  whois \
  cups \
  cups-client \
  cups-bsd \
  cups-filters \
  foomatic-db-compressed-ppds \
  printer-driver-all \
  openprinting-ppds \
  hpijs-ppds \
  hp-ppd \
  hplip \
  smbclient \
  printer-driver-cups-pdf \
&& apt-get clean \
&& rm -rf /var/lib/apt/lists/*

# Agregar usuario y deshabilitar la verificación de contraseña de sudo
RUN useradd \
  --groups=sudo,lp,lpadmin \
  --create-home \
  --home-dir=/home/print \
  --shell=/bin/bash \
  --password=$(mkpasswd print) \
  print \
&& sed -i '/%sudo[[:space:]]/ s/ALL[[:space:]]*$/NOPASSWD:ALL/' /etc/sudoers

# Configurar los servicios para que sean accesibles
RUN /usr/sbin/cupsd \
  && while [ ! -f /var/run/cups/cupsd.pid ]; do sleep 1; done \
  && cupsctl --remote-admin --remote-any --share-printers \
  && kill $(cat /var/run/cups/cupsd.pid)

# Parche el archivo de configuración predeterminado para habilitar el cifrado solo si se solicita
RUN sed -e '0,/^</s//DefaultEncryption IfRequested\n&/' -i /etc/cups/cupsd.conf

# Shell predeterminado
CMD ["/usr/sbin/cupsd", "-f"]
